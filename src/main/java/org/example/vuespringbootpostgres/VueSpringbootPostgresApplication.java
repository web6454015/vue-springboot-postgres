package org.example.vuespringbootpostgres;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VueSpringbootPostgresApplication {

    public static void main(String[] args) {
        SpringApplication.run(VueSpringbootPostgresApplication.class, args);
    }

}
